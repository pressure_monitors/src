import smbus
import time
import os
import sys
import urllib            # URL functions
import urllib2           # URL functions

import bmp180            # Sensor library
import RPi.GPIO as GPIO  # GPIO library

################# Default Constants #################
# These can be changed if required
DEVICE        = 0x77 # Default device I2C address
SMBUSID       = 1    # Rev 2 Pi uses 1, Rev 1 uses 0
LEDGPIO       = 17   # GPIO for LED
SWITCHGPIO    = 22   # GPIO for switch
INTERVAL      = 1    # Delay between each reading (mins)
AUTOSHUTDOWN  = 1    # Set to 1 to shutdown on switch
THINGSPEAKKEY = 'ABCDEFGH12345678'
THINGSPEAKURL = 'https://api.thingspeak.com/update'
#####################################################

def switchCallback(channel):

  global AUTOSHUTDOWN

  # Called if switch is pressed
  if AUTOSHUTDOWN==1:
    os.system('/sbin/shutdown -h now')
  sys.exit(0)

def sendData(url,key,field1,field2,temp,pres):
  """
  Send event to internet site
  """

  values = {'api_key' : key,'field1' : temp,'field2' : pres}

  postdata = urllib.urlencode(values)
  req = urllib2.Request(url, postdata)

  log = time.strftime("%d-%m-%Y,%H:%M:%S") + ","
  log = log + "{:.1f}C".format(temp) + ","
  log = log + "{:.2f}mBar".format(pres) + ","

  try:
    # Send data to Thingspeak
    response = urllib2.urlopen(req, None, 5)
    html_string = response.read()
    response.close()
    log = log + 'Update ' + html_string

  except urllib2.HTTPError, e:
    log = log + 'Server could not fulfill the request. Error code: ' + e.code
  except urllib2.URLError, e:
    log = log + 'Failed to reach server. Reason: ' + e.reason
  except:
    log = log + 'Unknown error'

  print log

def main():

  global DEVICE
  global SMBUSID
  global LEDGPIO
  global SWITCHGPIO
  global INTERVAL
  global AUTOSHUTDOWN
  global THINGSPEAKKEY
  global THINGSPEAKURL

  # Check if config file exists and overwrite
  # default constants with new values
  if os.path.isfile('/boot/templogger.cfg')==True:
    print "Found templogger.cfg"
    f = open('/boot/templogger.cfg','r')
    data = f.read().splitlines()
    f.close()
    if data[0]=='Temp Logger':
      print "Using templogger.cfg"
      DEVICE        = int(data[1],16)
      SMBUSID       = int(data[2])
      LEDGPIO       = int(data[3])
      SWITCHGPIO    = int(data[4])
      INTERVAL      = int(data[5])
      AUTOSHUTDOWN  = int(data[6])
      THINGSPEAKKEY = data[7]
      THINGSPEAKURL = data[8]

  # Setup GPIO
  GPIO.setmode(GPIO.BCM)
  GPIO.setwarnings(False)
  # LED on GPIO17
  GPIO.setup(LEDGPIO , GPIO.OUT)
  # Switch on GPIO22 as input pulled LOW by default
  GPIO.setup(SWITCHGPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
  # Define what function to call when switch pressed
  GPIO.add_event_detect(SWITCHGPIO, GPIO.RISING, callback=switchCallback)

  bus = smbus.SMBus(SMBUSID)

  try:

    while True:
      GPIO.output(LEDGPIO, True)
      (temperature,pressure)=bmp180.readBmp180(DEVICE)
      sendData(THINGSPEAKURL,THINGSPEAKKEY,'field1','field2',temperature,pressure)
      sys.stdout.flush()

      # Toggle LED while we wait for next reading
      for i in range(0,INTERVAL*60):
        GPIO.output(LEDGPIO, not GPIO.input(LEDGPIO))
        time.sleep(1)

  except :
    # Reset GPIO settings
    GPIO.cleanup()

if __name__=="__main__":
   main()


temp.py 
import smbus
import time
from ctypes import c_short
 
DEVICE = 0x77 # Default device I2C address
 
#bus = smbus.SMBus(0)  # Rev 1 Pi uses 0
bus = smbus.SMBus(1) # Rev 2 Pi uses 1 
 
def convertToString(data):
  # Simple function to convert binary data into
  # a string
  return str((data[1] + (256 * data[0])) / 1.2)

def getShort(data, index):
  # return two bytes from data as a signed 16-bit value
  return c_short((data[index] << 8) + data[index + 1]).value

def getUshort(data, index):
  # return two bytes from data as an unsigned 16-bit value
  return (data[index] << 8) + data[index + 1]

def readBmp180Id(addr=DEVICE):
  # Chip ID Register Address
  REG_ID     = 0xD0
  (chip_id, chip_version) = bus.read_i2c_block_data(addr, REG_ID, 2)
  return (chip_id, chip_version)
  
def readBmp180(addr=DEVICE):
  # Register Addresses
  REG_CALIB  = 0xAA
  REG_MEAS   = 0xF4
  REG_MSB    = 0xF6
  REG_LSB    = 0xF7
  # Control Register Address
  CRV_TEMP   = 0x2E
  CRV_PRES   = 0x34 
  # Oversample setting
  OVERSAMPLE = 3    # 0 - 3
  
  # Read calibration data
  # Read calibration data from EEPROM
  cal = bus.read_i2c_block_data(addr, REG_CALIB, 22)

  # Convert byte data to word values
  AC1 = getShort(cal, 0)
  AC2 = getShort(cal, 2)
  AC3 = getShort(cal, 4)
  AC4 = getUshort(cal, 6)
  AC5 = getUshort(cal, 8)
  AC6 = getUshort(cal, 10)
  B1  = getShort(cal, 12)
  B2  = getShort(cal, 14)
  MB  = getShort(cal, 16)
  MC  = getShort(cal, 18)
  MD  = getShort(cal, 20)

  # Read temperature
  bus.write_byte_data(addr, REG_MEAS, CRV_TEMP)
  time.sleep(0.005)
  (msb, lsb) = bus.read_i2c_block_data(addr, REG_MSB, 2)
  UT = (msb << 8) + lsb

  # Read pressure
  bus.write_byte_data(addr, REG_MEAS, CRV_PRES + (OVERSAMPLE << 6))
  time.sleep(0.04)
  (msb, lsb, xsb) = bus.read_i2c_block_data(addr, REG_MSB, 3)
  UP = ((msb << 16) + (lsb << 8) + xsb) >> (8 - OVERSAMPLE)

  # Refine temperature
  X1 = ((UT - AC6) * AC5) >> 15
  X2 = (MC << 11) / (X1 + MD)
  B5 = X1 + X2
  temperature = int(B5 + 8) >> 4

  # Refine pressure
  B6  = B5 - 4000
  B62 = int(B6 * B6) >> 12
  X1  = (B2 * B62) >> 11
  X2  = int(AC2 * B6) >> 11
  X3  = X1 + X2
  B3  = (((AC1 * 4 + X3) << OVERSAMPLE) + 2) >> 2

  X1 = int(AC3 * B6) >> 13
  X2 = (B1 * B62) >> 16
  X3 = ((X1 + X2) + 2) >> 2
  B4 = (AC4 * (X3 + 32768)) >> 15
  B7 = (UP - B3) * (50000 >> OVERSAMPLE)

  P = (B7 * 2) / B4

  X1 = (int(P) >> 8) * (int(P) >> 8)
  X1 = (X1 * 3038) >> 16
  X2 = int(-7357 * P) >> 16
  pressure = int(P + ((X1 + X2 + 3791) >> 4))

  return (temperature/10.0,pressure/100.0)

def main():
    
  (chip_id, chip_version) = readBmp180Id()
  print("Chip ID     : {0}".format(chip_id))
  print("Version     : {0}".format(chip_version))

  print
  
  (temperature,pressure)=readBmp180()
  print("Temperature : {0} C".format(temperature))
  print("Pressure    : {0} mbar".format(pressure))
  
if __name__=="__main__":
   main()




