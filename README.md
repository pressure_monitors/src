 import csv import lcddriver

display = lcddriver.lcd()

try: with open('cpu_temp.csv') as csv_file: # Replace cpu_temp.csv with file location of sensor data csv_reader = csv.reader(csv_file, delimiter=',')

line_count = 0
    while True:
        for row in csv_reader:
            label = "CPU Temp: "           # Replace label name with Pressure: and Temp:
            temp = row[1]
            display_temp = label + temp
            display.lcd_display_string(display_temp,1)
print(row[1])
line_count += 1
except KeyboardInterrupt: print("Program terminated.") display.lcd_clear()